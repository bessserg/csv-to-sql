from django.urls import path
from .views import add_country, add_region, add_city

urlpatterns = [
    path('add-country', add_country),
    path('add-region', add_region),
    path('add-city', add_city),
]