from django.contrib import admin
from import_export.admin import ImportExportActionModelAdmin
from modeltranslation.admin import TranslationAdmin

from core.models import Region, Country, City


class BaseTranslationImportExportAdmin(
    ImportExportActionModelAdmin, TranslationAdmin, admin.ModelAdmin
):
    search_fields = (
        "title_en",
        "title_ru",
        "title_es",
    )

    class Media:
        js = (
            "https://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js",
            "https://ajax.googleapis.com/ajax/libs/jqueryui/1.10.2/jquery-ui.min.js",
            "modeltranslation/js/tabbed_translation_fields.js",
        )
        css = {
            "screen": ("modeltranslation/css/tabbed_translation_fields.css",),
        }


class CountryAdmin(BaseTranslationImportExportAdmin):
    list_display = ('id', 'title',"title_en", "title_ru", "title_es",)


class RegionAdmin(BaseTranslationImportExportAdmin):
    list_display = ('id', "title_en", "title_ru", "title_es", "country")
    list_filter = ("country",)


class CityAdmin(BaseTranslationImportExportAdmin):
    list_display = (
        "title",
        "area",
        "region",
        "country",
    )
    list_filter = (
        "area",
        "country",
    )


admin.site.register(Region, RegionAdmin)
admin.site.register(Country, CountryAdmin)
admin.site.register(City, CityAdmin)
