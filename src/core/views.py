from django.http import HttpResponse
import csv
from .models import Country, City, Region


def add_country(request):
    with open('../data/_countries.csv', encoding='utf-8') as f:
        reader = csv.DictReader(f)
        for row in reader:
            Country.objects.create(id = row['id'], title_en=row['title_en'], title_ru=row['title_ru'], title_es=row['title_es'])
    return HttpResponse('<h1>Countries added</h1>')



def add_region(request):
    with open('../data/_regions.csv', encoding='utf-8') as f:
        reader = csv.DictReader(f)
        for row in reader:
            country = Country.objects.get(pk=row['country'])
            Region.objects.create(id = row['id'], title_en=row['title_en'], title_ru=row['title_ru'], title_es=row['title_es'], country=country)
    return HttpResponse('<h1>Regions added</h1>')



def add_city(request):
    countries = Country.objects.all()
    regions = Region.objects.all()
    dict_countries = {}
    for country in countries:
        dict_countries[country.id] = country
    dict_regions = {}
    for region in regions:
        dict_regions[region.id] = region
    with open('../data/_cities.csv', encoding='utf-8') as f:
        reader = csv.DictReader(f)
        lst = []
        counter = 0
        for row in reader:
            if counter == 10000:
                City.objects.bulk_create(objs=lst)
                lst.clear()
                counter= 0
            city = City(id=row['city_id'], title_en=row['title_en'], title_ru=row['title_ru'], title_es=row['title_es'], 
                area_en=row['area_en'], area_ru=row['area_ru'], area_es=row['area_es'], country=dict_countries[int(row['country_id'])])
            if row['region_id']:
                city.region = dict_regions[int(row['region_id'])] 
            lst.append(city)
            counter += 1
        City.objects.bulk_create(objs=lst)
    return HttpResponse('<h1>Cities added</h1>')
    
